package com.sampark.dsign.main;

import java.util.TimeZone;

import org.apache.log4j.Logger;

public class StartProcess {
	public static Logger logger = Logger.getLogger(StartProcess.class);
	
	/*static {
		BasicConfigurator.configure();
	}*/
	public static void main(String[] args) {
		
		logger.info("Application has started...");
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Calcutta"));
		
		MasterThread initThread = new MasterThread();
		initThread.execute();
		
	}
}
