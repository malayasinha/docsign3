package com.sampark.dsign.resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ResourceReader {
		private ResourceReader()
	{
	}
	public static String getValueFromBundle(String propertyKey,
		String resourceBundleName) {
		String value = null;
		ResourceBundle resourceBundle = null;
		try {
			try {
				resourceBundle = ResourceBundle.getBundle(resourceBundleName);

			} catch (MissingResourceException missingResourceExp) {
				//to do
			}
			value = resourceBundle.getString(propertyKey);
		} catch (MissingResourceException missingResourceExp) {
			missingResourceExp.printStackTrace();
			/*logger.info("Error while finding key :" + propertyKey
					+ " from bundle " + resourceBundleName);*/

		}
		return value;
	}
	public static String getValueFromPropertiesBundle(String propertyKey,String resourceBundleName) {
		String value = null;
		ResourceBundle resourceBundle = null;
		try {
			try {
				resourceBundle = new PropertyResourceBundle(new FileInputStream(resourceBundleName));
				value = resourceBundle.getString(propertyKey);
			} catch (MissingResourceException missingResourceExp) {

				missingResourceExp.printStackTrace();
			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (MissingResourceException missingResourceExp) {
		}catch(Exception e){
			e.printStackTrace();
		}
		return value;
	}
	
}
