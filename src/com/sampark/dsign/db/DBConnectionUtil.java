package com.sampark.dsign.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.sampark.dsign.exception.AppException;

public class DBConnectionUtil
{
	static Logger logger = Logger.getLogger(DBConnectionUtil.class);
	public Connection dbconn;

	public DBConnectionUtil() {
		this.dbconn = null;
	}

	public static Connection getDBConnection()
			throws Exception {
		Connection connection = null;
		FrameworkDBConnection dbConn = FrameworkDBConnection.getInstance();
		connection = dbConn.getDBConnection();
		if (connection == null)
		{
			logger.error(" Exception occured while getting connection.");
			throw new AppException(
					"Connection not found");
		}
		return connection;
	}
	public static void releaseResources(Connection connection) {
		try {
			if ((connection != null) && (!connection.isClosed())) {
				connection.close();

				connection = null;
			}
		}
		catch (SQLException sqlException) {
			logger.error(
					"  Could not close the connection. Reason:" + 
							sqlException.getMessage() + ". Error Code:" + 
							sqlException.getErrorCode(), sqlException);
		}
	}

	public static void releaseResources(Statement st, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (st == null)
				return;
			st.close();
			st = null;
		}
		catch (SQLException sqlException) {
			logger.error(" Could Not Close ResultSet and Statement. Reason:" + 
					sqlException.getMessage() + ". Error Code:" + 
					sqlException.getErrorCode(), sqlException);
		}
	}
}
