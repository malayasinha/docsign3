package com.sampark.dsign.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestClass {
	public static void main(String[] args) {
		List<File> fileList = SignatureUtil.getAssortedList("/home/malaya/input/", ".pdf");
		System.out.println(fileList);
		
		List<File> firstNElementsList = fileList.stream().limit(75).collect(Collectors.toList());
		
		ZipUtility zip = new ZipUtility();
		zip.createZipFileNIO("/home/malaya/output/", firstNElementsList,"sample");
		
	}
}