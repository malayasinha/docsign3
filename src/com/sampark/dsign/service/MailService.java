package com.sampark.dsign.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.dao.DBQueries;
import com.sampark.dsign.model.EmailContentBean;
import com.sampark.dsign.model.ProfileMailBean;
import com.sampark.dsign.util.SignatureUtil;

public class MailService {
	private static Logger logger = Logger.getLogger(MailService.class);
	DBQueries dao = new DBQueries();
	
	public void prepareMail(String type, Integer profileId,Integer[] message) {
		logger.info("Sending confirmation mail...");
		DBQueries dao = new DBQueries();
		String pattern = "dd-MM-yyyy HH:mm:ss";
		String todayAsString = SignatureUtil.getCurrentTimestamp(pattern);
		
		String email = getTemplateContent("info");
		email = StringUtils.replace(email, "{SUCCESS}", message[0].toString());
		email = StringUtils.replace(email, "{FAILED}", message[1].toString());
		email = StringUtils.replace(email, "{TOTAL}", message[2].toString());
		email = StringUtils.replace(email, "{DATE}", todayAsString);
		
		
		EmailContentBean mailContent = new EmailContentBean();
		mailContent.setBody(email.toString());
		
		mailContent.setEmailList(dao.getProfileMail(profileId));
		mailContent.setSubject("Sale Invoice & Credit Note");
		
		sendMail(mailContent);
		
	}
	
	public void prepareMail(String type, Integer profileId,String message) {
		String email = getTemplateContent("error");
		email = StringUtils.replace(email, "{MESSAGE}", message.toString());
		
		EmailContentBean mailContent = new EmailContentBean();
		mailContent.setBody(email.toString());
		mailContent.setEmailList(dao.getProfileMail(profileId));
		mailContent.setSubject("Digisign alert");
		
		sendMail(mailContent);
		
	}
	
	
	private void sendMail(EmailContentBean content) {
		String password = Constants.MAILPASS.equals("null")?null:Constants.MAILPASS;

		Properties props = new Properties();
		props.put("mail.smtp.host", Constants.MAILHOST); // SMTP Host
		props.put("mail.smtp.port", Constants.MAILPORT); // TLS Port
		props.put("mail.smtp.auth", Constants.MAILAUTH); // enable authentication
		//props.put("mail.smtp.starttls.enable", "true"); // enable STARTTLS

		// create Authenticator object to pass in Session.getInstance argument
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Constants.MAILUSERID,password);//password
			}
		};
		Session session = Session.getInstance(props, authenticator);
		logger.info(content);
		if(Constants.MAILENABLE.equals("Y")) {
			sendEmailTo(session, Constants.MAILEMAIL, content);
		} else {
			logger.info("Mail Functionality is disabled");
		}
		
	}

	private static void sendEmailTo(Session session, String fromEmail,EmailContentBean content) {
		try {
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			
			msg.setFrom(new InternetAddress(fromEmail, "Digisign"));
			//msg.setReplyTo(InternetAddress.parse(toEmail, false));
			
			//msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			if(content.getEmailList() != null) {
				for(ProfileMailBean bean:content.getEmailList()) {
					if(bean.getRecepientType().equals("TO")) {
						msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(bean.getEmailId(), false));
					} else {
						msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(bean.getEmailId(), false));
					}
				}
			}
						
			msg.setSubject(content.getSubject(), "UTF-8");
			msg.setContent(content.getBody(), "text/html; charset=utf-8");
			//msg.setText(body,"text/html; charset=utf-8");
			msg.setSentDate(new Date());
			msg.saveChanges();
			Transport.send(msg);

			System.out.println("EMail Sent Successfully!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String getTemplateContent(String type) {
		String fileName = type+".txt";
		
		File file = new File(Constants.projPath+fileName);
		FileReader fr=null;
		String line="";
		StringBuilder emailBody = new StringBuilder();
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			while((line = br.readLine()) != null){
			    emailBody.append(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return emailBody.toString();
	}
}
