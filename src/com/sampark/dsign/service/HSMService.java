package com.sampark.dsign.service;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import org.apache.log4j.Logger;

import com.safenetinc.luna.LunaSlotManager;
import com.safenetinc.luna.exception.LunaException;
import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.dao.DBQueries;
import com.sampark.dsign.model.SignatoriesBean;

public class HSMService {
	private static Logger logger = Logger.getLogger(DBQueries.class);
	private static LunaSlotManager hsmConnection = LunaSlotManager.getInstance();

	public static SignatoriesBean readPrivateKeyFromHSM(SignatoriesBean signatories) throws LunaException {
		String message="";
		if (Security.getProvider("LunaProvider") == null) {
			Security.insertProviderAt(new com.safenetinc.luna.provider.LunaProvider(), 2);
		}
		hsmConnection.login(Constants.PartitionName, Constants.PartitionPassword);

		KeyStore ks = null;
		PrivateKey privateKey = null;
		Certificate[] chain = new Certificate[1];
		Certificate certi = null;
		try {
			ks = KeyStore.getInstance("Luna");
			ks.load(null, Constants.PartitionPassword.toCharArray());
			
			privateKey = (PrivateKey) ks.getKey(signatories.getPrivateKeyLabel(), null);
			
			certi = ks.getCertificate(signatories.getCertificateLabel());
			chain[0] = certi;
		} 
		catch (IOException ioEx) {
			logger.info("Unable to log in to HSM Device: "+ioEx.getMessage());
			message = "HSM Device is down. Unable to reach HSM Device";
			ioEx.printStackTrace();
		} catch (UnrecoverableKeyException ukEx) {
			logger.info("Private Key or Certificate Key Corrupt: "+ukEx.getMessage());
			message = "Private Key or Certificate Key Corrupt: "+ukEx.getMessage();
			ukEx.printStackTrace();
		} catch (KeyStoreException ksEx) {
			logger.info("KeyStore instance creation failed: "+ksEx.getMessage());
			message = "KeyStore instance creation failed: "+ksEx.getMessage();
			ksEx.printStackTrace();
		} catch (NoSuchAlgorithmException nsaEx) {
			logger.info("Invalid Algorithm: "+nsaEx.getMessage());
			message = "Invalid Algorithm: "+nsaEx.getMessage();
			
			nsaEx.printStackTrace();
		} catch (CertificateException cEx) {
			logger.info("Invalid Certificate: "+cEx.getMessage());
			message = "Invalid Certificate: "+cEx.getMessage();
			cEx.printStackTrace();
		} catch (Exception ex) {
			logger.info("Exception occurred: "+ex.getMessage());
			message = "Exception occurred: "+ex.getMessage();
			ex.printStackTrace();
		}
		signatories.setMessage(message);
		if(privateKey == null || certi==null) {
			return signatories;
		} else {
			signatories.setPrivateKey(privateKey);
			signatories.setCert(chain);
			return signatories;
		}
		
	}
}
