package com.sampark.dsign.transfer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;

public class ShellService {
	Logger logger = Logger.getLogger(ShellService.class);
	public void transferFiles(String srcFolder) {
		logger.info("Transfer files to the sftp location.");
		try {
			File folder = new File(srcFolder);
			File[] listOfFiles = folder.listFiles();

			for (int c1 = 0; c1 < listOfFiles.length; c1++) {
				if (listOfFiles[c1].isFile()) {
					transferFile(listOfFiles[c1].getAbsolutePath());
					listOfFiles[c1].delete();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public String transferFile(String fileName) {
		String sftpPath = Constants.sftpPath;
		Process p;
		String line="";
		StringBuilder message = new StringBuilder(); 
		String[] cmd = { "sh", "/home/digisign/profile3/filecopy.sh", fileName, sftpPath};
		BufferedReader stdInput = null;
		BufferedReader stdError = null;
		try {
			
			p = Runtime.getRuntime().exec(cmd);
			stdInput = new BufferedReader(new 
				     InputStreamReader(p.getInputStream()));
			
			stdError = new BufferedReader(new 
				     InputStreamReader(p.getErrorStream()));
			
			p.waitFor();
			
			while ((line = stdInput.readLine()) != null) {
				message.append(line);
			}
			logger.info(line);
			while ((line = stdError.readLine()) != null) {
				message.append(line);
			}
			logger.error(line);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return message.toString();
	}
}
